import React from 'react'
import { Route,  BrowserRouter as Router } from 'react-router-dom'
import Renting from './Components/Renting';

export default class AppRoutes extends React.Component {
  render() {
    return (
      <Router>
        <Route path="/" component={Renting} />
      </Router>
    );
  }
}