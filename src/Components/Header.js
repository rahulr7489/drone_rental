

import React from 'react';
import {Navbar,Nav,NavDropdown} from 'react-bootstrap'

export default class Header extends React.Component {
  constructor(props) {    
      super(props);
      this.handleLogout = this.handleLogout.bind(this);
    }
  handleLogout(e){
    localStorage.removeItem('userDetails');
    localStorage.removeItem('isAuth');
    localStorage.removeItem('dronUserData');
    window.location.reload();
  }
  render(){
      let navBar;
      if(this.props.isAuth){
      navBar = (<span className="ml-auto"><Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav" >
                          
                          <Nav className="ml-auto">
                            
                            <NavDropdown title={this.props.userDetails.firstname} id="basic-nav-dropdown">
                              <NavDropdown.Item onClick={this.handleLogout}>Logout</NavDropdown.Item>
                              
                            </NavDropdown>
                          </Nav>
                        </Navbar.Collapse></span>)
    }
      return (
           <header className="main_header">
            <Navbar bg="light" expand="lg" className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
              <Navbar.Brand href="#home">Drone Renting System</Navbar.Brand>
              {navBar}
            </Navbar>
            </header>
    
      );
  }
}





