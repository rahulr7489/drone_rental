import React from 'react'
import * as data from '../Constants/Mock.json'
import * as users from '../Constants/Users.json'
import  '../App.css'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal'
import { find } from 'lodash';
import Header from './Header'



export default class Renting extends React.Component {
	constructor(props) {    
    	super(props);
    	this.state={
		      showModal:false,
		      email:null,
		      enablePassword: false,
		      isEmailDisabled: false,
		      password:null,
		      errorClass:null,
		      errorMsg:null,
		      isAuth:false,
		      userDetails:null,
		      data:data.quads,
		      dronUserData:null,
		      timer:0,
		      dronId:null,
		      isTimerstarted: false
		      
		      }
    	this.handleSelection = this.handleSelection.bind(this);	
    	this.handleClose = this.handleClose.bind(this);	
    	this.handleVerifyEmail = this.handleVerifyEmail.bind(this);
    	this.onEmailChange = this.onEmailChange.bind(this);
    	this.onPasswordChange = this.onPasswordChange.bind(this);
    	this.handleHiredrone = this.handleHiredrone.bind(this);
    	this.handleCharge = this.handleCharge.bind(this);
    	this.startTimer = this.startTimer.bind(this);
    	this.stopFlying = this.stopFlying.bind(this);
    	this.tick = this.tick.bind(this);
  	}
  	componentDidMount(){
    	if(localStorage.getItem('isAuth')){
    		let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    		let dronUserData = JSON.parse(localStorage.getItem('dronUserData'));
    		console.log(dronUserData);
    		this.setState({
    			isAuth:true,
			    userDetails:userDetails,
			    dronUserData:dronUserData
    		})
    	}
  	}
  	resetState(){
		this.setState({email:null,
				      enablePassword: false,
				      isEmailDisabled: false,
				      password:null,
				      errorClass:null,
				      errorMsg:null,
					})
  	}
  	handleSelection(e){
  		let charge = this.state.data[e.target.getAttribute('dronid')].charge;
	  	let charge_value = charge.match(/\d+/g);
	  	if(charge_value<10){
	  		alert("There is no enough charge, select another Drone");
	  	}else{
	  		this.setState({showModal:true, dronId:e.target.getAttribute('dronid')})
	  		this.resetState();
  		}
  	}
  	handleClose(e){
  		this.setState({showModal:false})
  	}
  	handleVerifyEmail(e){
  		if(this.validateField('email',this.state.email)){
	  		if(find(users.users, { 'email': this.state.email})){
	  			this.setState({isEmailDisabled: true,enablePassword: true, errorMsg:"", errorClass: ''})
	  		}else{
	  			this.setState({isEmailDisabled: false,enablePassword: false, errorMsg:"This Email Id is not Registered", errorClass: 'alert alert-warning'})
	  		}
  		}
  	}
  	onEmailChange(e){
  		this.setState({email:e.target.value})
  	}
  	onPasswordChange(e){
  		this.setState({password:e.target.value})
  		
  	}
  	handleHiredrone(e){
  		if(this.validateField('password',this.state.password)){
	  		if(find(users.users, { 'email': this.state.email, 'password': this.state.password})){
	  			let userDetails = find(users.users, { 'email': this.state.email, 'password': this.state.password});
	  			if(userDetails.status === 'active'){
		  			userDetails.password = '';
		  			let dronUserData = [];
		  			
		  			dronUserData.push(this.state.data[this.state.dronId]);
		  			localStorage.setItem('isAuth',true);
		  			localStorage.setItem('dronUserData',JSON.stringify(dronUserData));
		  			localStorage.setItem('userDetails',JSON.stringify(userDetails));
		  			let timer = dronUserData[0].maxFlightTime;
		  			let timer_value = timer.match(/\d+/g);
		  			let timer_type = timer.match(/[^0-9\.]+/g);
		  			let timer_second = this.toSecond(timer_type[0],timer_value[0]);
		  			this.setState({errorMsg:"Successfully Booked Drone", 
		  						   errorClass: 'alert alert-success',
		  						   isAuth: true,
		  						   userDetails: userDetails,
		  						   dronUserData:dronUserData,
		  						   timer:timer_second })
	  			} else {
	  				this.setState({errorMsg:"User is "+userDetails.status, errorClass: 'alert alert-danger'})
	  			}
	  			
	  		} else {
	  			this.setState({errorMsg:"Wrong Password", errorClass: 'alert alert-danger'})
	  		}
  		}
  	}
  	validateField(fieldName, value) {
	  let fieldValidationErrors;
	  let emailValid = this.state.emailValid;
	  let passwordValid = this.state.passwordValid;

	  switch(fieldName) {
	    case 'email':
	      emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
	      fieldValidationErrors = emailValid ? null : fieldName + ' is invalid';
	      break;
	    case 'password':
	      passwordValid = value.length >= 2;
	      fieldValidationErrors = passwordValid ? null: fieldName +  ' is wrong';
	      break;
	    default:
	      break;
	  }
	  this.setState({errorMsg: fieldValidationErrors,
	  				errorClass: 'alert alert-danger',
	                  emailValid: emailValid,
	                  passwordValid: passwordValid
	                });
	  if(fieldValidationErrors == null){
	  	return true;
	  } else {
	  	return false;
	  }
	}
	autoLogout(){
		localStorage.removeItem('userDetails');
	    localStorage.removeItem('isAuth');
	    localStorage.removeItem('dronUserData');
	    window.location.reload();
	}
	handleCharge(e){
		let dronUserData = this.state.dronUserData;
		dronUserData[0].charge = '100%';
		localStorage.setItem('dronUserData',JSON.stringify(dronUserData));
		this.setState({dronUserData:dronUserData});
	}
	tick () {
		if(this.state.timer !== 0){
	    	this.setState({timer: (this.state.timer - 1), isTimerstarted: true})
		}else{
			let userDetails = this.state.userDetails;
			userDetails.status = 'banned';
			this.setState({userDetails:userDetails});

			clearInterval(this.timer)
			alert("Drone Crashed");
			this.autoLogout();
			
		}
	 }
	startTimer () {
		clearInterval(this.timer)
	    this.timer = setInterval(this.tick, 1000)
	 }
	toSecond(type, value){
		if(type==='min'){
			return value * 60;
		}
	}
	stopFlying () {

	    clearInterval(this.timer)
	    this.autoLogout();
	 }
	render(){
	    let passwordField;
	    let modalButton;
	    let dronData = this.state.data;
	   

	    if(this.state.isAuth){
	    	dronData = this.state.dronUserData;
	    	
	    }

	    if (this.state.enablePassword) {
	      modalButton = ( <Button variant="primary" onClick={this.handleHiredrone}>
							     Get Drone
							  </Button>)
	      passwordField = (<div><label htmlFor="exampleInputEmail1">Password</label>
                     <input type="password" className="form-control" placeholder="Password" onChange={this.onPasswordChange} required /></div>);
	    } else {
	      passwordField = '';
	      modalButton = ( <Button variant="primary" onClick={this.handleVerifyEmail} >
							    Verify Email
							  </Button>)
	    }

	 	return(
	 		    <div className="container">
            	<Header isAuth={this.state.isAuth} userDetails={this.state.userDetails}/> 
	 		  	<div className="row main_div">
	 		  	    <div className="col-md-12">
	 		  			<h3>Hire Your Drone</h3>
	 		  		</div>
	 		  		{dronData.map((item, key) =>
	 		  			<div className="col-md-4" key={key}>
				          <div className="card mb-4 shadow-sm">
				            <div className="card-body">
				              <p className="card-text"><dt className="text-truncate"><span>Manufacturer: </span><span className="normal_text">{item.manufacturer}</span></dt></p>
				              <p className="card-text"><dt className="text-truncate"><span>Model: </span> <span className="normal_text">{item.model}</span></dt></p>
				              <p className="card-text"><dt className="text-truncate"><span>Charge: </span> <span className="normal_text">{item.charge}</span></dt></p>
				              {this.state.isAuth ?
				              	<p className="card-text">Time Remaining <span className="redText">{this.state.timer}</span> seconds</p>: ''
				              }
				              <div className="d-flex justify-content-between align-items-center">
				                {this.state.isAuth ? 
				                
				                <div> 
				                  {this.state.isTimerstarted ?
				                  	<button type="button" className="btn btn-sm btn-primary"   key={key} id={key} dronid={key}  onClick={this.stopFlying} >Stop</button> :
				                  <button type="button" className="btn btn-sm btn-primary"   key={key} id={key} dronid={key}  onClick={this.startTimer} >Start</button>
				              }
				                  <button type="button" className="btn btn-sm btn-secondary"    onClick={this.handleCharge} >Charge</button>
				                 
				                </div>
				               
				                
				               
				                :
				                <div className="btn-group">
				                  <button type="button" className="btn btn-sm btn-outline-secondary"  key={key} id={key} dronid={key}  onClick={this.handleSelection} >Get This Drone</button>
				                </div>
				                
				                }
				                <small className="text-muted">{item.maxFlightTime}</small>
				                
				              </div>
				            </div>
				          </div>
			          </div>
	 		  		)}
	 		  		{/********* Drone Renting Modal ********/}
	 		  		<Modal show={this.state.showModal} onHide={this.handleClose}>
					<Modal.Header closeButton>
					  <Modal.Title>Drone Renting</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="col-md-12">
						  <div className={this.state.errorClass} role="alert">
						  {this.state.errorMsg}
						  </div>
						  
				          <div className="card mb-12 shadow-sm">
				            <div className="card-body">
				              <div className="form-group">
                                       <label htmlFor="exampleInputEmail1">Email address</label>
                                       <input type="email" className="form-control" placeholder="Enter Registered email" onChange={this.onEmailChange} disabled={this.state.isEmailDisabled} required/>
                                       <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                                       {passwordField}
                                     </div>
                                {modalButton}   
				             
				            </div>
				          </div>	
			          </div></Modal.Body>
					
					</Modal>
				</div>
				</div>
			

	 		)
	 }


	}