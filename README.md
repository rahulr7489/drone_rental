# Drone Rental #

## Application info ##
* its a single page application , based on state change dom will load the elements
* All Data flow is maintained in localstorage and state.(there is no server as mentioned)
* Users mock data is created as per different criteria.(like user active and banned)
* once the application has been loaded, user has to select the drone which want to rent.
* selection is based on drone charge , if charge is less than 10% user is not allowed to select.
* if drone has enough charge select the drone , and login to fly the drone.
* once user loged in page will list only the rented drone.
* user can able to charge the drone.
* once user click on start button counter will start running 
* counter is starting from max flight time to 0 (minute converted to seconds and timer funtion)
* user can stop the flying using stop button and it will automatically loged off the user.
* if drone is flying after the time limit , it will crash and usef will get notificaton.

### How to Run the application ###
* git clone https://gitlab.com/rahulr7489/drone_rental.git
* cd drone_rental
* npm install
* npm start
* localhost:3000

